objects in concept form have
- relativity score (layers of seperation, things are either directly related, or recursively related via relation to things that are related to the thing)
- difficulty score (distance from 'home', whether or not the chest belongs to the person, perhaps)
- recency of search (for instance, how long it's been since searched a chest to see if others have stored things there)
