package io.gitlab.dahlterm.minya_fortress.implementation;

import net.minecraft.util.Identifier;

public class Toolbox {
public static Identifier newIdentifier(String suffix) {
	return new Identifier("minyafortress", suffix);
}
}
