package io.gitlab.dahlterm.minya_fortress.hookins.entitystuff;

import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.util.Identifier;

public class MinyaCoreEntityRenderer extends MobEntityRenderer<MinyaCoreEntity, MinyaCoreEntityModel> {
	public MinyaCoreEntityRenderer(EntityRendererFactory.Context context, MinyaCoreEntityModel entityModel, float f) {
		super(context, entityModel, f);
	}

	@Override
	public Identifier getTexture(MinyaCoreEntity entity) {
		return null;
	}
}
