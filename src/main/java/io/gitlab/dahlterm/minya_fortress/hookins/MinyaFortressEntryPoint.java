package io.gitlab.dahlterm.minya_fortress.hookins;

import io.gitlab.dahlterm.minya_fortress.hookins.entitystuff.MinyaCoreEntity;
import io.gitlab.dahlterm.minya_fortress.implementation.Toolbox;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.util.registry.Registry;
import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MinyaFortressEntryPoint implements ModInitializer {
// This logger is used to write text to the console and the log file.
// It is considered best practice to use your mod name as the logger's name.
// That way, it's clear which mod wrote info, warnings, and errors.
public static final Logger LOGGER = LoggerFactory
                                      .getLogger("minyafortress");

public static final EntityType<MinyaCoreEntity>
  MINYA_CORE_ENTITY_ENTITY_TYPE = Registry.register(
  Registry.ENTITY_TYPE,
  Toolbox.newIdentifier(
	"minya_core_entity"),
  FabricEntityTypeBuilder.create(
	  SpawnGroup.CREATURE,
	  MinyaCoreEntity::new)
	.dimensions(EntityDimensions
	              .fixed(0.75f, 0.75f)).build());

@Override
public void onInitialize(ModContainer mod) {
	LOGGER.info("Hello Quilt world from {}!", mod.metadata().name());
}
}
