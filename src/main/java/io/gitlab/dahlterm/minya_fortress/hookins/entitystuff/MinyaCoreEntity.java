package io.gitlab.dahlterm.minya_fortress.hookins.entitystuff;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.mob.PathAwareEntity;
import net.minecraft.world.World;

public class MinyaCoreEntity extends PathAwareEntity {
	public MinyaCoreEntity(EntityType<? extends PathAwareEntity> entityType, World world) {
		super(entityType, world);
	}
}
